# Terraform-lab2

#### LAB 2 du 03/03

****************************************************************
    Lab1 : Terraform Hashicorp Learn
****************************************************************
​
https://learn.hashicorp.com/terraform
​
****************************************************************
    Lab2 : Les Bases de Terraform
****************************************************************
1-Create AWS Profile
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html
$ mkdir /home/stagiaire/.aws
$ gedit credentials
$ cat credentials
[walidsaad]
aws_access_key_id=AKaa
aws_secret_access_key=Draaa
​
$ export AWS_PROFILE=walidsaad
​
2-Install Terraform & VSCode
https://www.terraform.io/downloads.html
​
3-Create lab directory
$ mkdir lab2
4-Create TF template for AWS Provider
$ cd lab2
$ cat provider.tf
terraform {
  
  required_providers{
      aws= {
          source = "hashicorp/aws"
          version = "~> 3.27"
      }
  }
}
provider "aws" {
    profile = "default"
    region ="us-east-1"
  
}
​
5-Download AWS Plugin
​
$ terraform init
​
6-Create EC2 Instance
$ cat ec2-instance.tf
resource "aws_instance" "example" {
  ami           = "ami-0a54aef4ef3b5f881"
  instance_type = "t2.micro"
​
  provisioner "local-exec" {
    command = "echo ${aws_instance.example.public_ip} > ip_address.txt"
  }
}
​
7-Execution plan
​
$terraform plan
​
8-Deploy Infrastructure
​
$ terraform apply
$ cat ip_address.txt
​
9-Destroy Infrastructure
​
$ terraform destroy
​
10-Update TF template (Create Key Pair, EC2 Instance and Remote Exec)
​
$ cat ec2-instance.tf
​
resource "aws_key_pair" "example" {
  key_name   = "examplekey"
  public_key = file("~/.ssh/id_rsa.pub")
}
​
resource "aws_instance" "example" {
  key_name      = aws_key_pair.example.key_name
  ami           = "ami-0915bcb5fa77e4892"
  instance_type = "t2.micro"
​
  provisioner "local-exec" {
    command = "echo ${aws_instance.example.public_ip} > ip_address.txt"
  }
​
  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file("~/.ssh/id_rsa")
    host        = self.public_ip
  }
​
  provisioner "remote-exec" {
    inline = [
      "sudo yum -y update",
      "sudo yum -y install httpd",
      "sudo systemctl enable httpd",
      "sudo systemctl start httpd"
    ]
  }
}
$ ssh-keygen -t rsa
$ chmod 400 ~/.ssh/
$ terraform plan
$ terraform apply
​
****************************************************************
    Lab3 : AWS Use Case: Amazon S3
****************************************************************
1-Create lab directory
​
$ mkdir lab3
​
2-Create TF template for AWS Provider
​
$ cd lab3
$ cat provider.tf
terraform {
  
  required_providers{
      aws= {
          source = "hashicorp/aws"
          version = "~> 3.27"
      }
  }
}
provider "aws" {
    profile = "default"
    region ="us-east-1"
  
}
​
3-Download AWS Plugin
​
$ terraform init
​
4-Create Bucket S3
$ cat s3-bucket.tf
resource "aws_s3_bucket" "my-bucket-example"{
   bucket = "my-bucket-example-09072000"
   acl = "public-read"
}
resource "aws_s3_bucket_object" "picture-of-cat" {
    bucket =  aws_s3_bucket.my-bucket-example.id
    key = "cat.jpg"
    acl = "public-read"
    source = "./cat.jpg"
}
​
$ terraform plan
$ terraform apply
$ terraform destroy
