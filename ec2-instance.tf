
resource "aws_key_pair" "clef_jl2" {
    key_name   = "clef_auto_jl"
    public_key = file("~/.ssh/id_rsa.pub")
    }
    
resource "aws_instance" "test_jl2" {
    key_name      = aws_key_pair.clef_jl2.key_name
    ami           = "ami-0ec28fc9814fce254"
    instance_type = "t2.micro"
    
    provisioner "local-exec" {
        command = "echo ${aws_instance.test_jl2.public_ip} > ip_address.txt"
    }

    connection {
        type        = "ssh"
        user        = "ec2-user"
        private_key = file("~/.ssh/id_rsa")
        host        = self.public_ip
    }

    provisioner "remote-exec" {
        inline = [
            "sudo yum -y update",
            "sudo yum -y install httpd",
            "sudo systemctl enable httpd",
            "sudo systemctl start httpd"
        ]
    }
}